import unittest
import numpy as np
import healpy as hp
import libpol as lp


class TestNPPAnalysis(unittest.TestCase):
    def test_gauss_nppa(self):
        """
        The heaviest test. Test the detection
        of non-polarized points in a Gaussian realization
        of the CMB.
        """

        ps_r005 = hp.read_cl("./demo/cl_TEB_r0.05000.fits")
        test_nppa = lp.NppAnalysis.from_power_spectrum(ps_r005, 17, 256, 2, np.radians(4), harmonics="Full")
        epsilon = 0.0234375

        # Test the non-polarized points found in 3 subareas
        test_res = test_nppa.npps_in_subarea(35, epsilon)
        self.assertEqual(test_res, ([575764, 580845], [575462], [574660, 576639, 579056, 580820]))

        test_res = test_nppa.npps_in_subarea(12, epsilon)
        self.assertEqual(
            test_res,
            (
                [197469, 200076, 201066],
                [197242, 198107],
                [197205, 198207, 200168, 200931, 208577],
            ),
        )

        test_res = test_nppa.npps_in_subarea(28, epsilon)
        self.assertEqual(test_res, ([464271, 466768], [465820], [463891, 466350]))
        self.assertEqual(test_nppa.npp_count(epsilon, supress_progress=True), 347)

    def test_daughters(self):
        """
        Test finding daughter pixels for a given mother
        at some nsides.
        """
        f = lambda x, y, z: list(lp.pixel.daughter_pixels(x, y, z))

        self.assertEqual(f(12, 4, 2), [48, 49, 50, 51])
        self.assertEqual(f(24, 8, 4), [96, 97, 98, 99])
        self.assertEqual(f(48, 16, 8), [192, 193, 194, 195])
        self.assertEqual(f(96, 32, 16), [384, 385, 386, 387])

    def test_calc_ratios(self):
        """
        Test ratio calculation and minimum number of non-polarized
        points required for calculation.
        """
        fs = [1, 2, 3, 4, 2]
        ks = [1, 2, 8, 9, 5]
        ss = [1, 2, 5, 1, 8]

        fk_ratio, fs_ratio, dropped = lp.calc_ratios(fs, ks, ss)
        self.assertTrue(all(fk_ratio == np.array([1.0, 0.375, 0.4])))
        self.assertTrue(all(fs_ratio == np.array([1.0, 0.6, 0.25])))
        self.assertEqual(dropped, 2)
        self.assertEqual(lp.calc_ratios(fs, ks, ss, min_npps=3), ([0.375], [0.6], 4))
