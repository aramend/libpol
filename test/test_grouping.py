import unittest
import numpy as np
import healpy as hp
import libpol as lp


class TestGrouping(unittest.TestCase):
    def setUp(self):
        ps_r005 = hp.read_cl("./demo/cl_TEB_r0.05000.fits")
        self.test_nppa = lp.NppAnalysis.from_power_spectrum(ps_r005, 17, 256, 2, np.radians(2), harmonics="Full")

    def test_angular_grouping(self):
        np.random.seed(17)
        all_daughters = lp.pixel.daughter_pixels(3, 256, 4)

        expectations = [
            [14688, 15753, 14273, 13811, 15900, 12488, 13446, 16323, 13853, 12548, 13180, 14051, 12737],
            [16224, 15002, 13903, 12974, 14688, 13318, 12423, 13955, 14282, 15740, 13681, 14572, 15298, 13056, 12402, 14438],
            [12766, 13983, 15659, 14700, 13096, 15699, 13717, 16227, 14853, 15946, 13859],
            [16323, 12576, 15342, 14338, 14515, 13010, 16047, 12307, 16121, 13472, 13851, 12425, 14234],
            [13241, 16182, 14697, 14570, 16379, 14143, 12589, 15139, 13997, 15939, 14338, 13879, 13079],
        ]

        for ix in range(5):
            subset = np.random.choice(all_daughters, 20, replace=False)
            cold_pixels = np.array(lp.pixel.angular_grouping(self.test_nppa.pi_map, subset, np.radians(2)))
            expect = np.array(expectations[ix])
            np.testing.assert_array_equal(expect, cold_pixels)

    def test_basin_grouping(self):
        self.assertTrue(True)
