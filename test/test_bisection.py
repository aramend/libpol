import unittest
import numpy as np
import healpy as hp
import libpol as lp

from ctypes import c_float


class TestBisection(unittest.TestCase):
    def test_bisection_finds_best_epsilon(self):
        ps_r005 = hp.read_cl("./demo/cl_TEB_r0.05000.fits")
        test_spa = lp.NppAnalysis.from_power_spectrum(ps_r005, 17, 32, 2, np.radians(6), harmonics="Full")
        epsilons, _ = test_spa.bisect_epsilon(0, 0.1, cutoff=1, supress_progress=True)
        actual_count = test_spa.npp_count(epsilons[-1], supress_progress=True)
        self.assertTrue(abs(actual_count - test_spa.expected_npp_count) < 1)
