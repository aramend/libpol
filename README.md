# libpol
Library for analysis of singular points in a CMB polarization field.

### Main workflow
This is `nppa.count` or `nppa.count_in(mother_pix)`.
1. Create NPPAnalysis object
2. Call the global counting function.
    1. Multiprocessing setup with subarea queue **Add threads kwarg**
    2. Each worker creates basin, why?
    3. Each worker creates dict from name to map array, why?
    4. While subareas in queue
    5. Count NPPs in subarea:
        1. Count dense NPPs in subarea
            1. Find pixels that intersect below epsilon (Lives on basin object)
            2. Group them into disjoint basins (Lives on basin object)
            3. Keep those that have crossing points in both fields (Lives on basin object)
            4. Find local minima
            5. Classify and return local minima
        2. Perform angular grouping on each type of NPP

### **TODO**
1. Write tests
    - Unit test everything that goes into create_spaobj and everything in SpAnalysis
    - spa.angular_grouping

2. Redesign package structure

3. Other stuff:
- Singular Points -> NPPs
- Abstract out angular_grouping and disjoint_basins
- PEP-compliant docs.
- Write a demo notebook for example usage of the library.
    - Full sky analysis
    - Partial sky analysis
    - Polarisation mask usage
