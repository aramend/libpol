"""Functions for grouping and classifying non-polarized points."""
from typing import Tuple, Iterable
from cmath import sqrt
from operator import attrgetter

import numpy as np

from ._types import Pixels

# TODO: Better argument names
def calc_ratios(
    fs: Iterable[int], ks: Iterable[int], ss: Iterable[int], min_npps: int = 2
) -> Tuple[np.ndarray, np.ndarray, int]:
    """Given iterables of counts of NPPs in analysed subareas, calculate the NPP ratios in each subarea. This requires that each index in the input counts corresponds to the same subarea.

    Arguments:
        fs {Iterable[int]} -- The counted foci in several subareas.
        ks {Iterable[int]} -- The counted knots in several subareas.
        ss {Iterable[int]} -- The counted saddles in several subareas.

    Keyword Arguments:
        min_npps {int} -- The minimum number of NPPs that should be present in a subarea for a ratio to be evaluated. (default: {2})

    Returns:
        Tuple[np.ndarray, np.ndarray, int] -- f/k ratios, f/s ratios and the number of subareas where ratios were not evaluated.
    """
    if not len(fs) == len(ks) == len(ss):
        raise ValueError("Count lists differ in length.")

    if not all(isinstance(counts, np.ndarray) for counts in [fs, ks, ss]):
        fs = np.array(fs)
        ks = np.array(ks)
        ss = np.array(ss)

    bad_idxs = set()
    for sps in [fs, ks, ss]:
        bad_idxs = bad_idxs.union(set(np.where(sps < min_npps)[0]))

    bad_idxs = list(bad_idxs)

    if bad_idxs:
        fs = np.delete(fs, bad_idxs)
        ks = np.delete(ks, bad_idxs)
        ss = np.delete(ss, bad_idxs)

    return fs / ks, fs / ss, len(bad_idxs)


def classify_npps(
    pixels: Pixels,
    q1_map: np.ndarray,
    q2_map: np.ndarray,
    u1_map: np.ndarray,
    u2_map: np.ndarray,
) -> Tuple[Pixels, Pixels, Pixels]:
    """Given a collection of pixels and the Stokes parameters derivatives, classify what type of NPP each pixel is.

    Arguments:
        pixels {Pixels} -- The pixels corresponding to NPPs to classify.
        q1_map {np.ndarray} -- The Q1 derivative sky map.
        q2_map {np.ndarray} -- The Q2 derivative sky map.
        u1_map {np.ndarray} -- The U1 derivative sky map.
        u2_map {np.ndarray} -- The U2 derivative sky map.

    Returns:
        Tuple[Pixels, Pixels, Pixels] -- A tuple of Pixels. The first contains the focuses, the second the knots and the third the saddles.
    """
    npps = [], [], []
    for pix in pixels:
        q1v = q1_map[pix]
        q2v = q2_map[pix]
        u1v = u1_map[pix]
        u2v = u2_map[pix]
        npp_type = _classify_npp(q1v, q2v, u1v, u2v)
        if npp_type == -1:
            raise ValueError(f"Unclassified non-polarized point at {pix}")
        npps[npp_type].append(pix)
    return map(np.array, npps)


def _classify_npp(q1: float, q2: float, u1: float, u2: float) -> int:
    """Classify one non-polarized point based on its field derivatives.

    Arguments:
        q1 {float} -- The dphi derivative of the Q field.
        q2 {float} -- The dtheta derivative of the Q field.
        u1 {float} -- The dphi derivative of the U field.
        u2 {float} -- The dtheta derivative of the U field.

    Returns:
        int -- One of 0, 1 or 2. Corresponding to either a focus, knot or saddle. -1 will be returned if something weird happens.
    """
    m = (q1 + u2) / 2
    discriminant = sqrt(m ** 2 - (q1 * u2 - q2 * u1))
    eigenvals = [m + discriminant, m - discriminant]
    eigenvals.sort(key=_get_real)
    r1, r2 = map(_get_real, eigenvals)
    i1, i2 = map(_get_imag, eigenvals)
    if i1 == -i2 != 0:
        return 0
    if i1 == i2 == 0:
        if r1 * r2 > 0:
            return 1
        if r1 * r2 < 0:
            return 2
    return -1


_get_real = attrgetter("real")
_get_imag = attrgetter("imag")
