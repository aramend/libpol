"""
Module for general useful utilities related to the project.
"""
from typing import Tuple

import numpy as np
import healpy as hp
from IPython.display import clear_output

to_nest = lambda skymap: hp.reorder(skymap, r2n=True)


def map_derivatives(skymap, lmax=None):
    nside = hp.get_nside(skymap)
    alms = hp.map2alm(skymap, lmax=lmax)
    _, dtheta, dphi = hp.alm2map_der1(alms, nside)
    return dtheta, dphi


def eb_split(
    tqu: np.ndarray,
    lmax: int,
    separate_emode: bool = False,
    separate_bmode: bool = False,
) -> Tuple[np.ndarray, np.ndarray]:
    """Separate the Q and U maps of either the E or B family.

    Arguments:
        tqu {np.ndarray} -- A 3 * N_pix array of TQU maps.
        lmax {int} -- The maximum multipole moment for spherical transformations.

    Keyword Arguments:
        separate_emode {bool} -- If True, return the E family of QU maps. (default: {False})
        separate_bmode {bool} -- If True, return the B family of QU maps. (default: {False})

    Returns:
        Tuple[np.ndarray, np.ndarray] -- The Q and U maps of the target spherical harmonics.
    """
    if separate_bmode + separate_emode == 0:
        raise ValueError("Choose whether to separate the E or B family.")

    nside = hp.npix2nside(tqu.shape[1])

    alms = hp.map2alm(tqu, lmax=lmax, pol=True)
    alms0 = np.zeros(len(alms[0]), dtype="complex")

    if separate_emode:
        emode = hp.alm2map(
            [alms[0], alms[1], alms0], nside=nside, lmax=lmax, pol=True
        )
        return emode[1], emode[2]

    if separate_bmode:
        bmode = hp.alm2map(
            [alms[0], alms0, alms[2]], nside=nside, lmax=lmax, pol=True
        )
        return bmode[1], bmode[2]


def ac_pmask_at_nside(pmask, target_nside):
    """
    Get an AC (Area Conserving) polarization mask.
    Given a binary polarization mask, downgrade it such
    that the ratio of available sky to unavailable sky
    (call this q) is conserved, by discarding all pixels
    below the qth percentile value.
    """

    if np.sqrt(len(pmask) / 12) < target_nside:
        raise ValueError("Target NSIDE larger than input map NSIDE.")

    if not hp.isnsideok(target_nside, nest=True):
        raise ValueError("Target NSIDE not valid.")

    q = 1 - np.mean(pmask)
    downgraded = hp.ud_grade(pmask, target_nside)

    qth_percentile_val = np.percentile(downgraded, 100 * q)

    ret = np.zeros(downgraded.shape, dtype=int)
    ret[downgraded > qth_percentile_val] = 1

    return ret


def safe_lmax(smoothing_angle: float, deg: bool = True) -> int:
    """
    Given a smoothing angle, what is the LMAX
    that is still safe to use, but hopefully saves time
    on moving between pixel/harmonics domains.
    """
    if not deg:
        smoothing_angle = np.degrees(smoothing_angle)

    return int(3 * (180 / smoothing_angle))


def compatible_bins(data_iterable, n_bins):
    """
    Given an iterable that contains several data,
    find n_bins bins that can can be used for
    all data in data_iterable.

    Parameters
    ---
    data_iterable : iterable
        Iterable of data we want to find bins for.
    n_bins : int
        How many bins do we want.

    Returns
    ---
    bins : ndarray
        Array of n_bins bins to be used
        for plotting histograms.

    """
    # Find min/max values
    min_val = 1e10
    max_val = -1e10

    for data in data_iterable:

        current_min = np.min(data)
        current_max = np.max(data)

        min_val = current_min if (current_min < min_val) else min_val
        max_val = current_max if (current_max > max_val) else max_val

    n_bins = n_bins if isinstance(n_bins, int) else int(n_bins)
    bins = np.linspace(min_val, max_val, num=n_bins + 1)

    return bins


def histogram_xy(bins, data):
    """
    Take bins and a list/array of data,
    return a tuple of x and y coordinates to
    plot in a step chart.

    Parameters
    ---
    bins : list/ndarray
        Bins to use, they conform to numpy histogram
        usage.
    data : list/ndarray
        The data we want to get a histogram of.

    Returns
    ---
    x, y : (ndarray, ndarray)
        Tuple of numpy arrays, that will be used as
        the x and y in a step plot. Use mid=True when
        plotting these using ax.step().
    """
    y, _ = np.histogram(data, bins=bins, density=True)
    x = 0.5 * (bins[1:] + bins[:-1])

    return x, y


def smooth_tqu(tqu, fwhm, lmax=None):
    """
    Smooth an TQU set of maps.

    Parameters
    ---
    tqu : ndarray
        A (3, N) shaped numpy array.
    fwhm : float
        fwhm to use for smoothing the map, in radians.

    Returns
    ---
    smoothed_iqu : (ndarray, ndarray, ndarray)
        Smoothed tuple of T Q U maps.
    """
    t, q, u = tqu[0], tqu[1], tqu[2]
    nside = hp.get_nside(t)
    if lmax is None:
        TEB_alms = hp.smoothalm(
            hp.map2alm((t, q, u)), fwhm=fwhm
        )
    else:
        TEB_alms = hp.smoothalm(
            hp.map2alm((t, q, u), lmax=lmax), fwhm=fwhm
        )
    # RING ordering
    t, q, u = hp.alm2map(TEB_alms, nside)

    ret = np.empty(tqu.shape)
    ret[0, :] = t
    ret[1, :] = q
    ret[2, :] = u

    return ret
