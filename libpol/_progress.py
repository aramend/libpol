"""
Report evaluation progress to the user.
"""
from typing import Union


def update_progress(progress: Union[float, int], bar_length: int = 20, pct_spacing: int = 12) -> str:
    if isinstance(progress, int):
        progress = float(progress)
    if progress < 0:
        progress = 0
    if progress >= 1:
        progress = 1

    current_bar_length = int(round(bar_length * progress))
    return "[{0}] {1:>{2}.1f}%".format(
        "#" * current_bar_length + " " * (bar_length - current_bar_length), progress * 100, pct_spacing
    )
