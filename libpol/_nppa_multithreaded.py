"""
Module with classes to support finding non-polarized points
and analyzing their dynamics.
"""
import multiprocessing as mp
from math import sqrt, pi
from ctypes import c_uint32, c_float
from typing import Iterable, Tuple, Optional, List
from numbers import Real
from numpy.typing import ArrayLike
from IPython.display import clear_output

import numpy as np
import healpy as hp

from libpol.pixel import (
    daughter_pixels,
    disjoint_basins,
    pixel_local_minimum,
    angular_grouping,
    sub_epsilon_intersect,
)
from ._types import Pixels
from ._npp import classify_npps
from ._progress import update_progress
from ._utilities import (
    smooth_tqu,
    safe_lmax,
    map_derivatives,
    eb_split,
    ac_pmask_at_nside,
)

_DEFAULT_MSG_SIZE = 40


class NppAnalysis:
    """
    Class for analyzing NPPs in a nested HEALPix structure. Uses multiprocessing for efficient counting of NPPs.
    """

    def __init__(self, sp_nside, q_map, u_map, q1_map, q2_map, u1_map, u2_map, sa_idxs=None):
        """
        Input maps should be numpy arrays in a nested order!
        """

        # Parameters related to map sizes
        self.__sp_nside = sp_nside
        self.__data_nside = hp.get_nside(q_map)

        if not self._nsides_okay():
            raise ValueError("sp_nside too large compared to data_nside.")

        # List of subarea pixels outside the polarisation mask
        # for a given product.
        if not sa_idxs is None:
            # Use pixels outside polarisation mask
            self.__sa_idxs = sa_idxs
        else:
            # Use all pixels in sky
            self.__sa_idxs = np.arange(12 * sp_nside**2)

        self.__sa_count = len(self.__sa_idxs)

        # Initialize maps
        self.__q_map = q_map
        self.__u_map = u_map
        self.__q1_map = q1_map
        self.__u1_map = u1_map
        self.__q2_map = q2_map
        self.__u2_map = u2_map
        self.__pi_map = np.sqrt(q_map**2 + u_map**2)

        # Identify data pixels available for analysis
        data_idxs = np.concatenate([daughter_pixels(sa, self.__data_nside, sp_nside) for sa in self.__sa_idxs])

        # Calculate spectral parameters
        sigma_0q = np.mean(np.power(q_map[data_idxs], 2))
        sigma_0u = np.mean(np.power(u_map[data_idxs], 2))
        sigma_1q1 = 2 * np.mean(np.power(q1_map[data_idxs], 2))
        sigma_1u1 = 2 * np.mean(np.power(u1_map[data_idxs], 2))
        sigma_1q2 = 2 * np.mean(np.power(q2_map[data_idxs], 2))
        sigma_1u2 = 2 * np.mean(np.power(u2_map[data_idxs], 2))

        self.__sigma0 = sqrt((sigma_0q + sigma_0u) / 2)
        self.__sigma1 = sqrt((sigma_1q1 + sigma_1u1 + sigma_1q2 + sigma_1u2) / 4)
        self.__rc = self.__sigma0 / self.__sigma1

        # Average angular distance between non-polarized points
        self.__dist_focus = sqrt((16 * pi * self.__rc**2) / sqrt(2))
        self.__dist_knot = sqrt((16 * pi * self.__rc**2) / (sqrt(2) * (sqrt(2) - 1)))
        self.__dist_saddle = sqrt(8 * pi * self.__rc**2)

    @property
    def data_nside(self) -> int:
        """The NSIDE of the maps under investigation."""
        return self.__data_nside

    @property
    def sp_nside(self) -> int:
        """The NSIDE at which one pixel corresponds to one subarea in which NPPs are counted."""
        return self.__sp_nside

    @property
    def sa_idxs(self) -> Pixels:
        """The pixels in which NPPs are counted."""
        return self.__sa_idxs

    @property
    def sigma0(self) -> float:
        """The variance in the Q and U maps. In Gaussian skies they are expected to be equal."""
        return self.__sigma0

    @property
    def sigma1(self) -> float:
        """The variance in the derivatives of the Q and U maps. In Gaussian skies they are expected to be equal."""
        return self.__sigma1

    @property
    def rc(self) -> float:
        """The radius of correlation in the skies under analysis."""
        return self.__rc

    @property
    def q_map(self) -> np.ndarray:
        """The Stokes Q map."""
        return self.__q_map

    @property
    def q1_map(self) -> np.ndarray:
        """The dphi derivative of the Stokes Q map."""
        return self.__q1_map

    @property
    def q2_map(self) -> np.ndarray:
        """The dtheta derivative of the Stokes Q map."""
        return self.__q2_map

    @property
    def u_map(self) -> np.ndarray:
        """The Stokes U map."""
        return self.__u_map

    @property
    def u1_map(self) -> np.ndarray:
        """The dphi derivative of the Stokes U map."""
        return self.__u1_map

    @property
    def u2_map(self) -> np.ndarray:
        """The dtheta derivative of the Stokes U map."""
        return self.__u2_map

    @property
    def pi_map(self) -> np.ndarray:
        """The polarization intensity map."""
        return self.__pi_map

    @property
    def expected_npp_count(self) -> float:
        """Number of non-polarized points we expect to find in the subareas we are considering. Found from the computed spectral parameters."""

        return (1 / self.__rc**2) * (self.__sa_count / (12 * self.__sp_nside**2))

    def npp_count(self, epsilon: float, supress_progress: bool = False, **kwargs) -> int:
        """How many NPPs are counted at some value of epsilon. Does not distinguish between different types of NPPs, only gives the total count.

        Arguments:
            epsilon {float} -- The upper I limit for a local minimum to be considered a NPP.

        Returns:
            int -- The number of NPPs counted.
        """
        kwargs = {"supress_progress": supress_progress, **kwargs}
        return sum(map(sum, self.npps_all_subareas(epsilon, **kwargs)))

    def npps_in_subarea(self, subarea_pixel: int, epsilon: float, div: Real = 3) -> Tuple[Pixels, Pixels, Pixels]:
        """The main algorithm for counting non-polarized points (NPPs) in a given subarea corresponding to some HEALPix pixel.

        Arguments:
            subarea_pixel {int} -- The pixel to count NPPs in.
            epsilon {float} -- The upper I limit for a local minimum to be considered a NPP.

        Keyword Arguments:
            div {Real} -- Factor applied to the expected angular distance between each type of NPP. If two NPPs are closer than ang_dist_NPP/div, then only the colder of the two is kept. (default: {5})

        Returns:
            Tuple[Pixels, Pixels, Pixels] -- A tuple of foci, knots and saddles found inside the subarea.
        """
        current_daughters = daughter_pixels(subarea_pixel, self.__data_nside, self.__sp_nside)

        # 1. Identify all pixels in the subarea with field amplitudes LESS THAN epsilon
        sub_epsilon_pixels = sub_epsilon_intersect(epsilon, current_daughters, self.pi_map)

        # 2. Identify connected regions of sub-epsilon pixels
        basins = disjoint_basins(sub_epsilon_pixels, self.__data_nside)

        # 3. Keep only the sub-epsilon regions that have both positive and negative field values
        for basin_label in list(basins):
            pixs = basins[basin_label]
            labels_q = np.sign(self.q_map[pixs])
            labels_u = np.sign(self.u_map[pixs])

            if not ({1, -1}.issubset(labels_q) and {1, -1}.issubset(labels_u)):
                del basins[basin_label]

        # Find local minima in sub-epsilon regions
        locmins = np.unique(
            list(
                map(
                    lambda x: pixel_local_minimum(x, self.pi_map),
                    sum(basins.values(), []),
                )
            )
        )

        # Remove local minima that are outside the subarea
        isin_daughters = lambda pix: pix in current_daughters
        daughters_mask = list(map(isin_daughters, locmins))
        locmins = locmins[daughters_mask]

        # Classify the local minima
        all_npps = classify_npps(locmins, self.__q1_map, self.__q2_map, self.__u1_map, self.__u2_map)

        # 4. In the sub-epsilon regions, keep only the local minima for which
        # there are no colder local minima closer than alpha_max / div
        group_func = lambda npps, dist: angular_grouping(self.pi_map, npps, dist / div)

        npp_dist_zip = zip(
            all_npps,
            [self.__dist_focus, self.__dist_knot, self.__dist_saddle],
        )

        return tuple(group_func(ns, d) for ns, d in npp_dist_zip)

    # TODO: Check default div
    def npps_all_subareas(
        self,
        epsilon: Real,
        subareas: Optional[Pixels] = None,
        div: Real = 5,
        supress_progress: bool = False,
        threads: int = 8,
        msglist: List[str] = [],
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """Classify and count NPPs in all available subareas. Returns a triplet of arrays.

        Arguments:
            epsilon {Real} -- The upper I limit for a local minimum to be considered a NPP.

        Keyword Arguments:
            subareas {Optional[Pixels]} -- Subareas to identify NPPs in. If None, defaults to all available subareas. (default: {None})
            div {Real} -- Factor applied to the expected angular distance between each type of NPP. If two NPPs are closer than ang_dist_NPP/div, then only the colder of the two is kept. (default: {5})
            supress_progress {bool} -- If True, do not display a progress bar. (default: {False})
            threads {int} -- The number of threads to parallelize this work on. Set this to the number of threads available on your computer, for faster results. (default: {8})

        Returns:
            Tuple[np.ndarray, np.ndarray, np.ndarray] -- Numpy arrays all n_subareas long. The first represents the count of foci in each subarea, the second is for the count of knots, and the last is for the count of saddles.
        """
        if epsilon < 0:
            raise ValueError(f"Epsilon should be a positive number, not {epsilon}.")

        if not mp.get_start_method() == "fork":
            mp.set_start_method("fork")

        if subareas is None:
            subareas = self.__sa_idxs.copy()
            n_subareas = self.__sa_count
        else:
            n_subareas = len(subareas)

        timeout = 1 / (n_subareas // threads)

        fc, kc, sc = [mp.Array(c_uint32, [0] * n_subareas) for _ in range(3)]
        _exception: BaseException = None
        _prog = mp.Value(c_uint32, 0)

        if not supress_progress:
            msg_size = len(msglist[-1]) if msglist else _DEFAULT_MSG_SIZE
            bar_size = int(msg_size * 0.66)
            pct_size = msg_size - bar_size - 4

        def counter_proc(idxs: Iterable[int], sas: Pixels) -> None:
            nonlocal fc, kc, sc, _prog, _exception
            try:
                for idx, sa in zip(idxs, sas):

                    fs, ks, ss = self.npps_in_subarea(sa, epsilon, div)

                    fc[idx] = len(fs)
                    kc[idx] = len(ks)
                    sc[idx] = len(ss)
                    _prog.value += 1

            except Exception as e:
                _exception = e

        try:
            procs = [
                mp.Process(target=counter_proc, args=(idxs, sas)) for idxs, sas in _split_subareas(subareas, threads)
            ]
            [proc.start() for proc in procs]
            while any(proc.is_alive() for proc in procs):
                [proc.join(timeout) for proc in procs]
                if _exception:
                    raise _exception
                if not supress_progress:
                    print(
                        "\n".join(
                            msglist
                            + [update_progress(_prog.value / n_subareas, bar_length=bar_size, pct_spacing=pct_size)]
                        )
                    )
                    clear_output(wait=True)
        finally:
            [proc.terminate() for proc in procs]
            if _exception:
                raise _exception

        _mp_arr_to_nparr = lambda arr: np.array(list(arr))
        return tuple(map(_mp_arr_to_nparr, [fc, kc, sc]))

    def _nsides_okay(self) -> bool:
        """
        Returns True if data_nside is at least twice as large as sp_nside:
        """
        return self.__data_nside // self.__sp_nside >= 2

    def bisect_epsilon(
        self,
        epsilon_lower: float,
        epsilon_upper: float,
        max_iterations: int = 10,
        cutoff: float = 10.0,
        supress_progress: bool = False,
    ) -> Tuple[List[float], List[float]]:
        """
        Find the value for epsilon that results in a realized NPP count that mathces theory, using an iterative bisection method. Iterating ends either when max_iterations has been reached or when the difference between the theoretical count of NPPs and how many we actually counted given an epsilon value is smaller than cutoff.

        Arguments:
            epsilon_lower {float} -- The starting lower epsilon value.
            epsilon_upper {float} -- The starting upper epsilon value.

        Keyword Arguments:
            max_iterations {int} -- The number of iterations after which to terminate (default: {10})
            cutoff {float} -- The largest allowable difference between expected and realized NPP counts. If a smaller difference is found, iteration ends. (default: {100.0})
            show_progress {bool} -- Display progress. (default: {True})

        Returns:
            Tuple[List[float], List[float]] -- A tuple of the list of epsilon values at which NPP counts were evaluated, and a list of differences between theoretical and realized NPP counts.
        """
        f = (
            lambda epsilon, msglist: self.npp_count(epsilon, supress_progress=supress_progress, msglist=msglist)
            - self.expected_npp_count
        )

        lower = epsilon_lower
        upper = epsilon_upper
        f_lower = f(lower, [f"Evaluating at lower bound {lower}.."])
        f_upper = f(upper, [f"Evaluating at upper bound {upper}.."])

        if f_lower * f_upper > 0:
            raise ValueError(f"Bad bounds. Function evaluated at ({lower} -> {f_lower}) and ({upper} -> {f_upper})")

        hist_xvals = []
        hist_fvals = []

        item_size = int(_DEFAULT_MSG_SIZE * 0.75)
        value_size = _DEFAULT_MSG_SIZE - item_size - 1

        for i in range(1, max_iterations + 1):
            last_fval = -1 if not hist_fvals else hist_fvals[-1]
            mid = (lower + upper) / 2
            msgs = [
                "{0:{2}} {1:>{3}}".format("Iteration no", i, item_size, value_size),
                "{0:{2}} {1:>{3}.3f}".format("Current epsilon", mid, item_size, value_size),
                "{0:{2}} {1:>{3}.2f}".format("Last dist from expectation", last_fval, item_size, value_size),
            ]

            f_val = f(mid, msgs)

            hist_xvals.append(mid)
            hist_fvals.append(f_val)

            if np.abs(f_val) < cutoff:
                break
            if f_val < 0:
                lower = mid
            if f_val > 0:
                upper = mid

        return np.array(hist_xvals), np.array(hist_fvals)

    @staticmethod
    def from_power_spectrum(
        power_spectrum: ArrayLike,
        seed: int,
        data_nside: int,
        subarea_nside: int,
        fwhm_rad: float,
        harmonics: str = "full",
    ) -> "NppAnalysis":
        """Create a NppAnalysis object from a power spectrum, with constraints, for analysing NPPs in Gaussian simulations of CMB skies.

        Arguments:
            power_spectrum {ArrayLike} -- A power spectrum suitable for input in healpy.synfast. We use the 'new' cls layout.
            seed {int} -- Random seed for map generation.
            data_nside {int} -- The NSIDE to generate maps at.
            subarea_nside {int} -- The NSIDE at which one pixel defines one subarea.
            fwhm_rad {float} -- FWHM for smoothing the maps under analysis, In radians.

        Keyword Arguments:
            harmonics {str} -- Choose harmonics to analyse. Can be one of {"full", "e", or "b"}. For "full", no action is taken. For "e", the B-mode alms are set to zero, and the inverse is true when choosing "b".  (default: {"full"})

        Returns:
            NppAnalysis -- The object facilitating NPP counting.
        """
        harmonics = _validate_harmonics(harmonics)
        np.random.seed(seed)

        # RING ordering
        tqu = hp.synfast(power_spectrum, data_nside, pol=True, new=True)

        max_lmax = safe_lmax(fwhm_rad, deg=False)
        tqu = smooth_tqu(tqu, fwhm_rad, lmax=max_lmax)

        return _build_nppa(tqu, harmonics, subarea_nside, max_lmax)

    @staticmethod
    def from_fits_file(
        fits_filepath: str,
        data_nside: int,
        subarea_nside: int,
        fwhm_rad: float,
        harmonics: str = "full",
        tqup_indexes: Tuple[int, int, int, int] = (0, 1, 2, 4),
    ) -> "NppAnalysis":
        """Create a NppAnalysis object from a FITS file, with constraints, for analysing NPPs in the input CMB skies.

        Arguments:
            fits_filepath {str} -- A filepath pointing to a FITS file, which will be passed on to healpy.read_map. It is expected to have I, Q, U and P channels at indexes 0, 1, 2 and 4 will be converted to RING ordering. Maps are expected to be in units of K, which is converted to muK. If your FITS file differs in this, make corresponding changes to the tqup_indexes.
            data_nside {int} -- The NSIDE to generate maps at.
            subarea_nside {int} -- The NSIDE at which one pixel defines one subarea.
            fwhm_rad {float} -- FWHM for smoothing the maps under analysis, In radians.

        Keyword Arguments:
            harmonics {str} -- Choose harmonics to analyse. Can be one of {"full", "e", or "b"}. For "full", no action is taken. For "e", the B-mode alms are set to zero, and the inverse is true when choosing "b".  (default: {"full"})
            tqup_indexes {Tuple[int, int, int, int]} -- [description] (default: {(0, 1, 2, 4)})

        Returns:
            NppAnalysis -- The object facilitating NPP counting.
        """
        harmonics = _validate_harmonics(harmonics)

        # RING ordering
        tqup = hp.read_map(fits_filepath, (0, 1, 2, 4))
        tqup = hp.ud_grade(tqup, data_nside)
        tqu = tqup[:-1, :] * 1e6
        p = tqup[-1, :]
        del tqup

        max_lmax = safe_lmax(fwhm_rad, deg=False)
        tqu = smooth_tqu(tqu, fwhm_rad, lmax=max_lmax)

        # Figure out target subareas from polarization mask
        p_ac = ac_pmask_at_nside(p, subarea_nside)
        p_downgraded = hp.reorder(p_ac, r2n=True)
        sa_idxs = np.where(p_downgraded)[0]

        # TQU multiplied with smoothed polarization map ([0, 1]) to prevent edge effects
        # TODO: Warning! Smoothing T map with polarization mask!
        tqu[1:, :] *= hp.smoothing(p, fwhm_rad)

        return _build_nppa(tqu, harmonics, subarea_nside, max_lmax, sa_idxs)


def _validate_harmonics(family: str) -> str:
    """Check if chosen family of harmonics is legitimate. If it is, return lowercase version."""
    acceptable = ["full", "e", "b"]
    if not family.lower() in acceptable:
        raise ValueError(f'Chosen harmonics ({family}) is not one of {{"full", "e", or "b"}}.')
    return family.lower()


def _build_nppa(
    tqu: ArrayLike,
    harmonics: str,
    subarea_nside: int,
    max_lmax: int,
    sa_idxs: Optional[Iterable[int]] = None,
) -> NppAnalysis:
    """Given a set of maps in RING ordering describing the CMB polarization and target harmonics, build a NppAnalysis object."""
    if harmonics == "full":
        q2, q1 = map_derivatives(tqu[1, :], lmax=max_lmax)
        u2, u1 = map_derivatives(tqu[2, :], lmax=max_lmax)

    elif harmonics == "e":
        qe, ue = eb_split(tqu, max_lmax, separate_emode=True)
        q2, q1 = map_derivatives(qe, lmax=max_lmax)
        u2, u1 = map_derivatives(ue, lmax=max_lmax)

    elif harmonics == "b":
        qb, ub = eb_split(tqu, max_lmax, separate_bmode=True)
        q2, q1 = map_derivatives(qb, lmax=max_lmax)
        u2, u1 = map_derivatives(ub, lmax=max_lmax)

    return NppAnalysis(
        sp_nside=subarea_nside,
        q_map=hp.reorder(tqu[1, :], r2n=True),
        u_map=hp.reorder(tqu[2, :], r2n=True),
        q1_map=hp.reorder(q1, r2n=True),
        u1_map=hp.reorder(u1, r2n=True),
        q2_map=hp.reorder(q2, r2n=True),
        u2_map=hp.reorder(u2, r2n=True),
        sa_idxs=sa_idxs,
    )


def _split_subareas(sas: Pixels, n: int) -> List:
    """Split a list of subareas up evenly among n processes."""
    ret = []
    sas_per_proc = len(sas) // n
    for i in range(n):
        start = i * sas_per_proc
        stop = (i + 1) * sas_per_proc
        idxs = range(start, stop)
        if i == n - 1:
            ret.append((idxs, sas[start:]))
        else:
            ret.append((idxs, sas[start:stop]))
    return ret
