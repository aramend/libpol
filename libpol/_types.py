"""Type aliases that are used in the library."""
from typing import Iterable

"""Pixels are iterables of integers, which are indexes for some HEALPix map."""
Pixels = Iterable[int]
