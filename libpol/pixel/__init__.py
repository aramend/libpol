"""
Functions that deal with searches of connected regions and Pixels in relation to epsilon values.
"""
from numbers import Integral

import numpy as np
import healpy as hp

from libpol._types import Pixels


def daughter_pixels(mother_pix: Integral, data_nside: Integral, sp_nside: Integral) -> Pixels:
    """Given a mother pixel at sp_nside, return the pixels at data_nside that are inside it.

    Arguments:
        mother_pix {Integral} -- [description]
        data_nside {Integral} -- [description]
        sp_nside {Integral} -- [description]

    Returns:
        Pixels -- The pixels at data_nside that are inside mother_pix.
    """
    pixs_per_sa = data_nside**2 // sp_nside**2
    return np.arange(mother_pix * pixs_per_sa, (mother_pix + 1) * pixs_per_sa)


# TODO: This is a small function. Do I need it?
def sub_epsilon_intersect(epsilon: float, pixels: Pixels, pi_map: np.ndarray) -> Pixels:
    """Given an epsilon value and a collection of pixels, give back only those pixels where the polarization intensity I is valued less than epsilon.

    Arguments:
        epsilon {float} -- The epsilon parameter, the upper limit for field intensity.
        pixels {Pixels} -- The collection of pixels we are selecting matching ones from.
        pi_map {np.ndarray} -- The polarization intensity map.

    Returns:
        Pixels -- Pixels for which I < epsilon.
    """
    return pixels[pi_map[pixels] < epsilon]


# TODO: Better documentation
def disjoint_basins(sub_epsilon_pixels: Pixels, nside: int) -> dict:
    """
    Find disjoint connected areas in sub_epsilon_pixels
    and return a dictionary that maps a label to each
    such disjoint area.
    """
    # STEP 1: Map each pixel to a label and record equivalent labels
    pix2label = {}
    equiv_labels = {}
    current_label = 1

    for pix in sub_epsilon_pixels:
        # TODO: Filter out neighbours that are not in sub_epsilon_pixels
        nbrs = list(filter(lambda x: x != -1, hp.get_all_neighbours(nside, pix, nest=True)))
        nbr_labels = []

        for nbr in nbrs:
            if nbr in sub_epsilon_pixels:
                # Does this neighbour have a label?
                if nbr in pix2label.keys():
                    nbr_labels.append(pix2label[nbr])

        if len(nbr_labels) == 0:
            pix2label[pix] = current_label
            equiv_labels[current_label] = {current_label}
            current_label += 1
            continue

        pix2label[pix] = np.min(nbr_labels)

        # Add new equivalent labels to set
        nbr_labels_set = set(nbr_labels)
        for label in nbr_labels_set:
            equiv_labels[label] = equiv_labels[label].union(nbr_labels_set)

    # STEP 2: Reduce equivalent labels to minimum
    for label, equivalent in equiv_labels.items():
        equiv_labels[label] = min(equivalent)

    # STEP 3: Create a mapping of pixel -> label and
    # return it
    label2pix = {}
    for pixel, label in pix2label.items():
        minimum_equivalent = equiv_labels[label]

        if minimum_equivalent in label2pix:
            pixlist = label2pix[minimum_equivalent]
            pixlist.append(pixel)
            label2pix[minimum_equivalent] = pixlist
        else:
            label2pix[minimum_equivalent] = [pixel]

    return label2pix


def pixel_local_minimum(pixel: int, target_map: np.ndarray) -> int:
    """Given a pixel as the starting location, find the local minimum recursively.

    Arguments:
        pixel {int} -- The starting pixel for taking steps towards smaller values in target_map
        target_map {np.ndarray} -- The map in which we seek the local minimum for the pixel.

    Returns:
        int -- The pixel corresponding to a local minimum in target_map.
    """
    nside = hp.get_nside(target_map)

    def _pixel_local_minimum(pixel):
        nbrs = [pixel] + list(hp.get_all_neighbours(nside, pixel, nest=True))
        nbrs = list(filter(lambda x: x != -1, nbrs))

        target_vals = target_map[nbrs]
        next_min = nbrs[np.argmin(target_vals)]
        if next_min == pixel:
            return pixel
        return _pixel_local_minimum(next_min)

    return _pixel_local_minimum(pixel)


def angular_grouping(pi_map, sp_pixels, max_angdist):
    """
    Function that takes a list of one type of non-polarized points
    and a cutoff angular distance by which to group them. Pixels
    within the angular distance of each other are grouped, and
    only the coldest of them is kept, the rest are dropped from
    consideration.
    """
    data_nside = hp.get_nside(pi_map)
    labelled_sets = {}
    current_label = 1

    for sp in sp_pixels:
        close_pixels = angular_neighbours(data_nside, sp, sp_pixels, max_angdist)
        found_set = False
        for pixel in close_pixels:
            for label, pixel_set in labelled_sets.items():
                if pixel in pixel_set:
                    new_set = pixel_set.union(set(close_pixels))
                    labelled_sets[label] = new_set
                    found_set = True
                    break
            if found_set:
                break
        if not found_set:
            labelled_sets[current_label] = set(close_pixels)
            current_label += 1
    ret = []
    for pixel_set in labelled_sets.values():
        pixels = list(pixel_set)
        pis = pi_map[pixels]
        ret.append(pixels[np.argmin(pis)])
    return ret


def angular_neighbours(nside, source_pixel, neighbour_candidates, max_angdist):
    """
    Function that returns a subset of the candidate pixels whose centers
    are within max_angdist of the source_pixel.

    Parameters
    ---
    nside : int
        The resolution of the map we seek neighbours in.
    source_pixel : int
        A nested pixel whose neighbours we are looking for.
    neighbour_candidates : ndarray
        An array of candidate pixels we filter for being
        close enough to source_pixel.
    max_angdist : float
        Maximum allowed angular distance in radians between source_pixel
        and a candidate.

    Returns
    ---
    neighbours : list
        A list of pixels from neighbour_candidates that are within max_angdist
        of source_pixel.
    """
    dists = np.array([dist_rad(nside, source_pixel, cand) for cand in neighbour_candidates])
    close_mask = dists <= max_angdist

    return np.append(neighbour_candidates[close_mask], source_pixel)


def dist_rad(nside, pix_1, pix_2):
    """
    Angular distance between pixels pix_1 and pix_2.
    """
    v_1 = hp.pix2vec(nside, pix_1, nest=True)
    v_2 = hp.pix2vec(nside, pix_2, nest=True)
    dp = np.clip(np.dot(v_1, v_2), -1, 1)
    return np.arccos(dp)
