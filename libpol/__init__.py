"""libpol is a module to analyse non-polarized points in a CMB polarization field."""
from ._nppa_multithreaded import NppAnalysis
from ._npp import calc_ratios

from ._utilities import *

# TODO: non-polarized -> nonpolarized everywhere.
