import setuptools

from os import path

with open("README.md", "r") as fh:
    long_description = fh.read()

requires = [
    "numpy>=1.16"
    "healpy"
]

setuptools.setup(
    name="libpol", # Replace with your own username
    version=open(path.join("libpol", "VERSION")).readlines()[0].strip(),
    author="Jaan Kasak",
    author_email="jaan.kasak@abzu.ai",
    description="Library for analysis of non-polarized points in the CMB polarization field.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/aramend/planckpol",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Linux",
    ],
    install_requires=requires,
    tests_require=requires,
    python_requires='>=3.6',
)
